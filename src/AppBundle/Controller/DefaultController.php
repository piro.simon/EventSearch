<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use DateTime;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('entry-point/unchanged.php');
    }

    /**
     * @Route("/results", name="results")
     */
    public function resultsAction(Request $request)
    {
        //temporary solution for adjusting diferent request value and DB value
        switch ($request->get('location')) {
          case 'losangeles':
              $location = "Los Angeles";
              break;
          case 'newyork':
              $location = "New York";
              break;
          case 'philadelphia':
              $location = "Philadelphia";
              break;
          default:
              $location = "";
        }

        $query = $this->getDoctrine()->getManager()->createQueryBuilder()
         ->select('e')
         ->from('AppBundle:Event', 'e')
         ->where('e.name LIKE :keyword')
         ->andWhere('e.finish > :now')
         ->setParameter('keyword', "%". $request->get('keyword') ."%")
         ->setParameter('now', new \DateTime());
        //get all if location not selected
        if ($location != "") {
            $query->andWhere('e.location = :location')
               ->setParameter('location', $location);
        }
        //get all if category not selected
        if ($request->get('category') != "") {
            $query->andWhere('e.category = :category')
               ->setParameter('category', $request->get('category'));
        }

        $results = $query->getQuery()->getResult();
        return $this->render('public/results.html.twig', array(
                        'keyword' => $request->get('keyword'),
                        'events' => $results
            ));
    }
}
